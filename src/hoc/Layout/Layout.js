import React, { Component } from 'react';
import Auxi from '../Auxi/Auxi';
import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class Layout extends Component {
  
  state = {
    showSideDrawer: false
  }
  
  sideDrawerClosedHandler = () => {
    this.setState({showSideDrawer: false});
  };
  
  sideDrawerOpenHandler = () => {
    this.setState({showSideDrawer: true});
  };
    
  render() {
    return (
      <Auxi>
        <Toolbar open={this.sideDrawerOpenHandler} />
        <SideDrawer 
          closed={this.sideDrawerClosedHandler}
          open={this.state.showSideDrawer}
          />
        <main className={classes.Content}>
          {this.props.children}
        </main>
      </Auxi>
    )
  }
}

export default Layout;