import React from 'react';

import classes from './CheckoutSummary.module.css';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';

const checkoutSummmary = (props) => {
  return (
    <div className={classes.CheckoutSummary}>
      <h1>We hope you like it!</h1>
      <div style={{width: '100%', margin: 'auto'}}>
        <Burger ingredients={props.ingredients} />
      </div>
      <Button 
        btnType="Danger"
        clicked={props.checkoutCancelled}
      >
        CANCEL
      </Button>
      <Button 
        btnType="Success"
        clicked={props.checkoutContinued}
      >
        CONTINUE
      </Button>
    </div>
  );
}

export default checkoutSummmary;